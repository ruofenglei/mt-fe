FROM node:lts as build-stage
WORKDIR /code
COPY package.json yarn.lock /code/
RUN yarn install
COPY . /code
RUN yarn run build

FROM nginx:stable-alpine
COPY --from=build-stage /code/dist /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
