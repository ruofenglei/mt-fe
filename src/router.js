import Vue from "vue"
import Router from "vue-router"
import Home from "./components/Home.vue"
import KidHome from "./components/kid/Home.vue"
import KidMenu from "./components/kid/Menu.vue"
import KidMap from "./components/kid/Map.vue"
import KidTask1 from "./components/kid/Task1.vue"
import KidTask2 from "./components/kid/Task2.vue"
import KidPhoto1 from "./components/kid/Photo1.vue"
import KidPhoto2 from "./components/kid/Photo2.vue"
import KidTakePhoto from "./components/kid/TakePhoto.vue"
import KidGreat from "./components/kid/Great.vue"
import KidPhotoDone from "./components/kid/PhotoDone.vue"
import KidHint from "./components/kid/Hint.vue"
import KidAnswer1 from "./components/kid/Answer1.vue"
import KidAnswer2 from "./components/kid/Answer2.vue"
import KidCongratulations from "./components/kid/Con.vue"
import KidFinish from "./components/kid/Finish.vue"

import ParentHome from "./components/parent/Home.vue"
import ParentTheme from "./components/parent/Theme.vue"
import ParentList1 from "./components/parent/List1.vue"
import ParentList2 from "./components/parent/List2.vue"
import ParentInformation1 from "./components/parent/Information1.vue"
import ParentInformation2 from "./components/parent/Information2.vue"
import ParentFreedom from "./components/parent/Freedom.vue"
import ParentNav1 from "./components/parent/Nav1.vue"
import ParentNav2 from "./components/parent/Nav2.vue"
import ParentInstruction1 from "./components/parent/Instruction1.vue"
import ParentInstruction2 from "./components/parent/Instruction2.vue"
import ParentContent from "./components/parent/Content.vue"
import ParentContent2 from "./components/parent/Content2.vue"
import ParentFinish from "./components/parent/Finish.vue"

Vue.use(Router)

export default new Router({
    routes: [
        { path: "/", component: Home },
        { path: "/home", component: Home },
        {
            path: "/kid/home",
            component: KidHome
        },
        {
            path: "/kid/menu",
            component: KidMenu
        },
        {
            path: "/kid/map",
            component: KidMap
        },
        {
            path: "/kid/task1",
            component: KidTask1
        },
        {
            path: "/kid/task2",
            component: KidTask2
        },
        {
            path: "/kid/photo1",
            component: KidPhoto1
        },
        {
            path: "/kid/photo2",
            component: KidPhoto2
        },
        {
            path: "/kid/take-photo",
            component: KidTakePhoto
        },
        {
            path: "/kid/photo-done",
            component: KidPhotoDone
        },
        {
            path: "/kid/great",
            component: KidGreat
        },
        {
            path: "/kid/hint",
            component: KidHint
        },
        {
            path: "/kid/answer1",
            component: KidAnswer1
        },
        {
            path: "/kid/answer2",
            component: KidAnswer2
        },
        {
            path: "/kid/congratulations",
            component: KidCongratulations
        },
        {
            path: "/kid/finish",
            component: KidFinish
        },
        {
            path: "/parent/home",
            component: ParentHome
        },
        {
            path: "/parent/theme",
            component: ParentTheme
        },
        {
            path: "/parent/list1",
            component: ParentList1
        },
        {
            path: "/parent/list2",
            component: ParentList2
        },
        {
            path: "/parent/information1",
            component: ParentInformation1
        },
        {
            path: "/parent/information2",
            component: ParentInformation2
        },
        {
            path: "/parent/freedom",
            component: ParentFreedom
        },
        {
            path: "/parent/nav1",
            component: ParentNav1
        },
        {
            path: "/parent/nav2",
            component: ParentNav2
        },
        {
            path: "/parent/instruction1",
            component: ParentInstruction1
        },
        {
            path: "/parent/instruction2",
            component: ParentInstruction2
        },
        {
            path: "/parent/content",
            component: ParentContent
        },
        {
            path: "/parent/content2",
            component: ParentContent2
        },
        {
            path: "/parent/finish",
            component: ParentFinish
        }
    ]
})
