import Vue from "vue"
import App from "./App.vue"
import VueNativeSock from "vue-native-websocket"
import router from './router'
Vue.config.productionTip = false
Vue.use(VueNativeSock, "wss://yetian.design/mtkeeper-ws/")



new Vue({
    router,
    render: h => h(App)
}).$mount("#app")
